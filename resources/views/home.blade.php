@extends('master')


@section('title', 'Homepage Title')


@section('internal_css')

<style>
	body {
		background-color: lightblue;
	}
</style>

@endsection


@section('main_content')
	<div class="container">
		<div class="row">
			<h1 class="col-12">Homepage Heading</h1>
		</div>
	</div>
@endsection