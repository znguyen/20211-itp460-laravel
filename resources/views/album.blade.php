@extends('master')


@section('title', 'Album Page')


@section('internal_css')

<style>
	body {
		background-color: yellow;
	}
</style>

@endsection


@section('main_content')
	<div class="container">
		<div class="row">
			<h1 class="col-12">Album Page Heading</h1>
		</div>

		<div class="row">
			<div class="col-12">

				<?php
					// var_dump($all_albums);

					foreach ($all_albums as $row) {
						echo $row->title;
						echo "<hr>";
					}

				?>

			</div>
		</div>

	</div>
@endsection