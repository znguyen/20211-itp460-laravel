<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Album;

class SongController extends Controller
{

	public function displayAlbums () {

		// return "displayAlbums in SongController";

		$album = new Album();

		// var_dump($album->all());

		// exit();

		// foreach( $album->all() as $row ){
		// 	echo $row;
		// 	echo "<hr>";
		// }

		// exit();

		return view('album', [
			'all_albums' => $album->all()
		]);

	}
	
}
